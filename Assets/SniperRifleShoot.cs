﻿using UnityEngine;
using System.Collections.Generic;
using UnityEngine.Networking;

public class SniperRifleShoot : NetworkBehaviour
{
	/*
	//TODO: Need to make this generic for other weapons.

	//NOTE: PLAYER SHOOT dictates if shot fired. This means that shots fired, even when animations playing.

	GameObject ch;
	//what is ch??? Is this PlayerWeapon?
	Animation gunAnim;
	AudioSource gunSound;
	bool isFiring = false;
	bool isADS = false;

	// Use this for initialization
	void Start ()
	{
		gunAnim = GetComponent<Animation> ();
		gunAnim.Play ("Draw");
		ch = GameObject.FindGameObjectWithTag ("Crosshair");
		ch.SetActive (true);
		gunSound = GetComponent<AudioSource> ();
	}

	// Update is called once per frame
	void Update ()
	{
		if (!gunAnim.isPlaying) {
			Debug.Log ("ANIMATION IS NOT PLAYING");
			if ((Input.GetButtonDown ("Fire1") || Input.GetMouseButtonDown (0))) {
				isFiring = true;
			} else if ((Input.GetButtonUp ("Fire1") || Input.GetMouseButtonUp (0))) {
				isFiring = false;
			}

			//Own Reload bit
			if (Input.GetKeyDown (KeyCode.R)) {
				gunAnim.Play ("Reload");
				gunAnim.PlayQueued ("Unload");
				GlobalAmmo.currentExtraAmmo = GlobalAmmo.currentAmmo - GlobalAmmo.maxClipSize;
				GlobalAmmo.currentAmmo = GlobalAmmo.maxClipSize;
			}

			List<string> gunAnimNames = new List<string> ();
			foreach (AnimationState state in gunAnim) {
				gunAnimNames.Add (state.name);
			}
			if (Input.GetKeyDown (KeyCode.R) && GlobalAmmo.currentExtraAmmo > 0 && GlobalAmmo.currentAmmo < GlobalAmmo.maxClipSize) {
				Reload ();
			}

			if (Input.GetButtonDown ("Fire2")) {
				if (isADS) {
					gunAnim.Play ("UnADS");
					isADS = false;
				} else {
					gunAnim.Play ("ADS");
					isADS = true;
				}
			}
	

			if ((Input.GetButtonDown ("Fire1") || Input.GetMouseButtonDown (0)) && GlobalAmmo.currentAmmo > 0) {
				AudioSource gunSound = GetComponent<AudioSource> ();
				if (isADS) {
					gunAnim.Play ("ADS Fire");
					gunAnim.PlayQueued ("UnADS");
					gunAnim.PlayQueued ("Unload");
					gunAnim.PlayQueued ("ADS");
				} else {
					gunAnim.Play ("Hipfire");
					gunAnim.PlayQueued ("Unload");
				}
				gunSound.Play ();
				GlobalAmmo.currentAmmo--;

			}
		}
	}

	public void Reload ()
	{
		GlobalAmmo.currentAmmo = GlobalAmmo.currentExtraAmmo + GlobalAmmo.currentAmmo;

		if (GlobalAmmo.currentAmmo >= GlobalAmmo.maxClipSize) {
			gunAnim.Play ("Reload");
			gunAnim.PlayQueued ("Unload");
			GlobalAmmo.currentExtraAmmo = GlobalAmmo.currentAmmo - GlobalAmmo.maxClipSize;
			GlobalAmmo.currentAmmo = GlobalAmmo.maxClipSize;
		}
	}
	*/
}
