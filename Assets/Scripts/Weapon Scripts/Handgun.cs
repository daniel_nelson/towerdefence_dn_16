﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Handgun : BaseWeapon {

	GameObject ch;

	// Use this for initialization
	void Start ()
	{	
		gunAnim = GetComponent<Animation> ();
		gunAnim.Play ("Draw");
		ch = GameObject.FindGameObjectWithTag ("Crosshair");
		ch.SetActive (true);
		gunSound = GetComponent<AudioSource> ();
	

	}

	// Update is called once per frame
	void Update ()
	{
		UpdateAmmo ("∞");

		if (!gunAnim.isPlaying) {
			//Debug.Log ("ANIMATION IS NOT PLAYING");
			if ((Input.GetButtonDown ("Fire1") || Input.GetMouseButtonDown (0))) {
				isFiring = true;
			} else if ((Input.GetButtonUp ("Fire1") || Input.GetMouseButtonUp (0))) {
				isFiring = false;
			}


			if (Input.GetButtonDown ("Fire2")) {
				if (isADS) {
					gunAnim.Play ("UnADS");
					isADS = false;
				} else {
					gunAnim.Play ("ADS");
					isADS = true;
				}
			}


			if ((Input.GetButtonDown ("Fire1") || Input.GetMouseButtonDown (0)) && currentAmmo > 0) {
				AudioSource gunSound = GetComponent<AudioSource> ();
				if (isADS) {
					gunAnim.Play ("ADSFire");
				} else {
					gunAnim.Play ("Hipfire");
				}
				((PlayerShoot)FindObjectOfType (typeof(PlayerShoot))).Shoot ();
				muzzleFlash.Play ();
				gunSound.Play ();

				Debug.Log ("1Ammo is now " + currentAmmo);
				Debug.Log ("2Ammo is now " + currentAmmo);



			}
		}


	}
}
